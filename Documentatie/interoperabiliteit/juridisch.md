---
title: Juridische interoperabiliteit
weight: 1
---
## Grondslag
De grondslag voor dit uitwisselprofiel is te vinden in de Wlz. Zie [Juridisch kader](https://kik-v.gitlab.io/review/Afsprakenset/docs/juridisch-kader/) rij 4 van de tabel, Wlz, §2 Taken van de Wlz-uitvoerder, artikel 4.2.1 tot en met 4.2.6.

Het doel van deze gegevensuitwisseling is om de afdelingen zorgbemiddeling van de zorgkantoren (en andere gecontracteerde partijen zoals onafhankelijk cliëntondersteuners) van de juiste informatie te voorzien om (toekomstige) cliënten, mantelzorgers en naasten, die op zoek zijn naar een passende plaats voor zorg, zo volledig mogelijk te kunnen adviseren. De zorgkantoren gebruiken de gegevens om de verschillende in gebruik zijnde keuzetools (o.a. Zorgatlas) te vullen.

De manier waarop praktisch invulling wordt gegeven aan zorgbemiddelingstaken verschilt per zorgkantoor want is afhankelijk van
het inkoopbeleid, strategische thema’s en het communicatieplan zorgbemiddeling (conform NZa beleidsregels: Beleidsregels
toezicht kader zorgplicht Wlz (TH/BR-021) en Beleidsregels Normenkader Wlz-uitvoerder (TH/BR-026)).


## Doorlevering
De gegevens worden (m.u.v. de algemene gegevens) op vestigingsniveau doorgeleverd aan Patiëntenfederatie Nederland. Patientenfederatie Nederland gebruikt deze data op ZorgkaartNederland en in de Keuzehulp Verpleeghuiszorg. Deze digitale tools bieden beide informatie over zorginstellingen voor de cliënt en zijn of haar naasten. De gegevens worden op ZorgkaartNederland en in de Keuzehulp Verpleeghuiszorg aangevuld met andere data, zoals ervaringen (totaalscore) vanuit de cliënt/ naasten. Al deze informatie gezamenlijk geeft transparantie over de verpleeghuiszorg in Nederland en kan de cliënt, zijn of haar mantelzorgers en naasten gebruiken om inzicht te krijgen in het aanbod van de verschillende zorgaanbieders.
