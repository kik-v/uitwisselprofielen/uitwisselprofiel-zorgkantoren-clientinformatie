---
title: Technische interoperabiliteit
weight: 4
---
Indien een zorgaanbieder beschikt over een datastation, kunnen de resultaten worden aangeleverd. De zorgaanbieder gebruikt de
volgende SPARQL queries voor aanlevering van de resultaten: [Technische omschrijving](Documentatie/Gevalideerde_vragen_technisch/). Het zorgkantoor haalt de resultaten op via de KIK-starter. Voor meer informatie hierover zie het [Publicatieplatform](https://kik-v-publicatieplatform.nl/).

Voor nieuwe zorgaanbieders die nog niet via de KIK-Starter kunnen aanleveren is een invulformulier beschikbaar. 