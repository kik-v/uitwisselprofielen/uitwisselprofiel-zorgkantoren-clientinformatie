---
title: Semantische interoperabiliteit
weight: 3
---
## Introductie
Op basis van verschillende onderzoeken onder cliënten, mantelzorgers en naasten (uitgevoerd door Patiëntenfederatie Nederland) en input vanuit zorgbemiddelaars bij zorgkantoren is deze gegevensset opgebouwd. Deze gegevensset bevat vragen over onder andere zorgaanbod, Wzd-locaties, huurprijs van wooneenheden, capaciteit en overige vragen.


## Algemene vragen
| Algemene vragen|
|---|
| **0.1 Wat is de naam van de organisatie?**                                                                                                                    |
| **0.2 Wat is het KvK-nummer van de organisatie?**                                                                                                             |
| **0.3 Wat is de AGB code van de organisatie?**                                                                                                                |
| **0.4 Wat is het NZa nummer van de organisatie?**                                                                                                             |
| **0.5 Wat is de postcode van de organisatie?**                                                                                                                |
| **0.6 Wat is het huisnummer van de organisatie?**                                                                                                             |
| **0.7 Wat is de naam van de vestiging?**                                                                                                                      |
| **0.8 Wat is het KvK-vestigingsnummer van de vestiging?**                                                                                                     |
| **0.9 Wat is de AGB code van de vestiging**                                                                                                               |
| **0.10 Wat is de postcode van de vestiging?**                                                                                                                 |
| **0.11 Wat is het huisnummer van de vestiging?**                                                                                                              |
| Deze gegevens zijn nodig om de juiste organisatie te identificeren, de juiste doelgroep te selecteren en om eventueel vestigingsgegevens te kunnen koppelen.  |

**Aggregatieniveau**

Het aggregatieniveau is per indicator verschillend. Bij elke indicator staat aangegeven of dit organisatie- of vestigingsniveau betreft.

## Thema: Zorgaanbod
| Thema Zorgaanbod|
|---|
| **1.1.1 Welke zorgprofielen heeft deze vestiging geleverd bij leveringsvorm Verblijf?**|
| **1.1.2 In welke woonplaats(en) zijn welke zorgprofielen geleverd bij leveringsvorm Volledig Pakket Thuis (VPT)?**|
| **1.1.3 In welke woonplaats(en) is welk zorgaanbod geleverd bij leveringsvorm Modulair Pakket Thuis (MPT)?**|
| **1.1.4 Welke zorgprofielen heeft deze vestiging geleverd bij de combinatie van leveringsvormen Deeltijdverblijf (DTV)?**|
| **1.1.5 In welke woonplaats(en) is zorg thuis middels een Persoonsgebonden Budget (PGB) geleverd?**|
| Doel en achtergrond: De zorgvrager, mantelzorger of naaste wil weten of het zorgaanbod van de organisatie/vestiging aansluit bij de zorgvraag.|

## Thema: Wzd/Wvggz

| Thema Wzd/Wvggz|
|---|
| **1.4.1 Welke vestigingen zijn als Wzd- of Wvggz-vestiging geregistreerd?**|
| **1.4.2 Welke vestigingen zijn als Wzd- of Wvggz-accommodatie geregistreerd?**|
| Doel en achtergrond: Kan een zorgvrager op de vestiging (blijven) wonen als hij/zij onder de Wzd/Wvggz valt (psychogeriatrische aandoening, CIZ indicatie met als grondslag psychogeriatrische aandoening of een gelijkgestelde aandoening als Korsakov, Huntington of NAH, indien er sprake is van regieverlies vergelijkbaar met een psychogeriatrische aandoening en dit kan leiden tot ernstig nadeel)?|

## Thema: Huurprijs wooneenheden

| Thema Huurprijs wooneenheden|
|---|
| **3.1 Wat is het aantal personen dat per vestiging kan wonen (capaciteit)?**|
| **1.5.2 Wat is het aantal wooneenheden per huurprijscategorie?**|
| Doel en achtergrond: De zorgvrager wil weten hoeveel hij/zij moet betalen om op een vestiging te kunnen wonen.|

## Thema: Capaciteit

| Thema Capaciteit|
|---|
| **3.1 Wat is het aantal personen dat per vestiging kan wonen (capaciteit)?**|
| Doel en achtergrond: Het aantal personen zegt iets over de omvang van de vestiging. Zorgvragers kunnen dit meenemen in het keuzeproces.|

## Thema: Overige

| Thema Overige|
|---|
| **99.1 In welke mate wordt rekening gehouden met de wensen rondom levenseinde door deze vestiging?**|
| Doel en achtergrond: Voor zorgvragers en verwanten is het belangrijk dat rekening gehouden wordt m.b.t. wensen rondom het levenseinde.|
| **99.2 In welke mate wordt rekening gehouden met de voedselvoorkeuren van de cliënt door deze vestiging?**|
| Doel en achtergrond: Voor zorgvragers is het belangrijk dat rekening wordt gehouden met de voedselvoorkeuren.|
| **99.3 Wat is de link naar het laatste kwaliteitsverslag van de zorgaanbieder?**|


## Definities informatievragen

**Algemene uitgangspunten**

Voor de berekening van de indicatoren en informatievragen in de verschillende uitwisselprofielen worden algemene uitgangspunten gehanteerd. Uitgangspunten die gelden voor specifieke indicatoren of informatievragen worden bij de functionele beschrijving van de betreffende indicator beschreven. Indicator-specifieke uitgangspunten gaan voor op algemene uitgangspunten van een uitwisselprofiel.

Voor alle uitwisselprofielen gelden onderstaande algemene uitgangspunten: [Algemene uitgangspunten](https://kik-v-publicatieplatform.nl/documentatie/Algemene%20uitgangspunten)


**Indicatoren**
De functionele beschrijving van de berekening per indicator staat hier: [functionele beschrijving](/Documentatie/Gevalideerde_vragen_functioneel/)

**Benodigde gegevenselementen**
De concepten, eigenschappen en relaties die nodig zijn om de indicatoren te beantwoorden staan hier: [technische beschrijving](/Documentatie/Gevalideerde_vragen_technisch/)