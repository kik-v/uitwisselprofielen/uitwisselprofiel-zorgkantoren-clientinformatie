---
title: Release- en versiebeschrijving
weight: 3
---
| **Releaseinformatie** |  |
|---|---|
| Release | 1.0.0-RC1-acc|
| Versie | 1.0.0-RC1, Deze versie wordt voorgelegd aan het Tactisch Overleg, ter voorbereiding op besluitvorming door de Ketenraad |
| Doel | Release 1.0.0-RC1 betreft de functioneel omschreven behoefte van cliënten aan keuzeinformatie over een verpleeghuiszorg(locatie), vertaald in een uitvraag van ketenpartijen om hen daarbij te helpen.|
| Doelgroep | Zorgkantoren; Patiëntenfederatie Nederland; Zorgaanbieders verpleeghuiszorg |
| Totstandkoming | Release 1.0.0-RC1 is, ondersteund door het programma KIK-V, door een vertegenwoordiging van zorgkantoren, Patiëntenfederatie Nederland en ActiZ ontwikkeld.|
| Inwerkingtreding | 25-03-2024 |
| Operationeel toepassingsgebied | Cliëntkeuzeinformatie, zorgbemiddeling |
| Status | Ter besluitvorming |
| Functionele scope | Release 1.0.0-RC1 omvat de vragen die voor cliënten nodig zijn ter ondersteuning van de keuze voor een passende plaats voor verpleeghuiszorg. Dit kan een vestiging of zorg thuis zijn.|
| Licentie | Creative Commons: Naamsvermelding-GeenAfgeleideWerken 4.0 Internationaal (CC BY-ND 4.0). |

