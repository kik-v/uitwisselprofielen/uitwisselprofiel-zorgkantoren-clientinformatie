---
title: Algemeen
weight: 2
---
# Algemeen

## Doel van de uitvraag
Het uitwisselprofiel Cliëntinformatie bevat keuze informatie voor kwetsbare ouderen, mantelzorgers en naasten ter ondersteuning van de keuze voor een passende plaats voor verpleeghuiszorg. Dit kan zorg op een vestiging of zorg thuis zijn.
