---
title: Toelichting
weight: 1
---
# Toelichting
In 2021 heeft Vektis in opdracht van de ketenpartijen (zorgkantoren, ActiZ, Patiëntenfederatie Nederland (PFN), VWS) op basis van de eerder ontwikkelde GZ vragenlijst een vragenlijst VV in concept ontwikkeld. Het programma KIK-V is eind 2021 aangesloten bij de overleggen en heeft mede op basis daarvan samen met VWS de memo ‘Wie wat waar in de zorg en etalage informatie’ (9 december 2021) opgesteld. Dit memo is geen besluitvormend document geweest, maar is besproken in het Tactisch Overleg.

De ketenpartijen hebben op basis van de memo uitgesproken dat de uitvraag rondom cliëntkeuzeinformatie voldoet aan de principes van KIK-V en daarom ook op die manier het liefst uitgewerkt zou moeten worden. De memo heeft een vervolg/ verdere uitwerking gekregen v.w.b. de “Wat-informatie” (3 maart 2022). 

In het voorjaar 2022 is door Patiëntenfederatie Nederland een analyse gedaan van de benodigde informatie voor cliënten en hun naasten. Tevens is gekeken naar de achterliggende tools (Keuzehulp, ZorgKaartNederland, Zorgatlas, etc.) die gebruik maken van deze informatie. Vanuit deze basis is in het najaar 2022 door de werkgroep (met vertegenwoordiging vanuit de zorgkantoren, Patiëntenfederatie Nederland en ActiZ) gewerkt aan het uitwisselprofiel. In 2023 en begin 2024 is de eerste versie van het Uitwisselprofiel verder uitgewerkt met een verder uitgebreide vertegenwoordiging vanuit de zorgkantoren. 
De werkgroep is gezamenlijk gekomen tot een set van 31 vragen die benodigd zijn voor de ondersteuning van de cliëntkeuze voor een verpleeghuis(locatie). Tijdens de uitwerking van deze vragen is gebleken dat niet alle vragen op de traditionele KIK-V wijze - d.w.z.: gegevens uit bestaande bronregistraties ontsluiten en voorzien van betekenis - beantwoord kunnen worden. Voor de overige 18 vragen wordt gekeken naar alternatieve manieren om de vragen beantwoord te krijgen. Opties die verkend worden zijn onder andere: 1) het inzetten van textmining, waarbij gebruik wordt gemaakt van informatie uit openbare bronnen en 2) het registreren van 'aanvullende gegevens' door de zorgaanbieder. 
Om het uitwisselprofiel toch op te kunnen leveren, heeft de werkgroep besloten om het uitwisselprofiel gefaseerd op te leveren. In de eerste versie van dit uitwisselprofiel zijn enkel de 13 vragen opgenomen die op de traditionele KIK-V wijze beantwoord kunnen worden. Parallel hieraan werkt de werkgroep aan de doorontwikkeling van dit uitwisselprofiel. De tweede versie van dit uitwisselprofiel zal de totale set van vragen bevatten.

## Gekozen afbakeningen/ onderdelen van de uitvraag/informatiebehoefte
In de eerste versie van dit uitwisselprofiel zijn vragen rondom de volgende thema's opgenomen: zorgaanbod, Wzd/Wvggz, huurprijs wooneenheden, capaciteit en overige.
Bij elke vraag is de afweging gemaakt of het antwoord op de vraag behulpzaam is in het keuzeproces van een toekomstig cliënt en of deze objectief genoeg te formuleren is. De definitief geformuleerde vragen zijn vervolgens aangevuld (waar mogelijk) met een meer technisch geformuleerde indicator.

## Besluiten ten aanzien van de informatievragen
Het uitwisselprofiel Zorgkantoren Ondersteuning cliëntkeuze verpleging en verzorging onderscheidt zich van de andere uitwisselprofielen, omdat de gevraagde gegevens altijd up-to-date beschikbaar dienen te zijn. Dit betekent dat de gegevens aangeboden moeten gaan worden zodra er iets in veranderd is. Zorgatlas, Keuzehulp Verpleeghuiszorg en ZorgkaartNederland zijn op deze manier voorzien van actuele informatie.

Enkele vragen over personeel zijn afgevallen, deze zijn niet relevant voor (toekomstige) cliënten in het keuzeproces van verpleeghuiszorg.

* Hoeveel verzorgenden/ verpleegkundigen werken er en op hoeveel bewoners?
* Inzet van vrijwilligers?
* Wat is het verzuim?
* Wat is het verloop van het personeel?

Beide vragen zijn overwogen om de zorgvrager inzicht te geven in hoe het qua zorgpersoneel geregeld is op de vestiging. De werkgroep heeft geconcludeerd dat deze vragen onvoldoende aansluiten bij de informatiebehoefte van de zorgvrager en derhalve niet opgenomen worden. Een anders geformuleerde vraag kan vanzelfsprekend wel als ervaringsvraag meegenomen worden door zorgvrager en/of familie/naasten tijdens een locatiebezoek.

* Wie wordt mijn hoofdbehandelaar op deze vestiging?

Het is belangrijk voor een zorgvrager om te weten of hij/zij zijn of haar eigen huisarts mag houden. Vanuit de bronregistratie bij zorgaanbieders geredeneerd: zodra een zorgvrager een indicatie “Verblijf met behandeling” krijgt, wordt de zorg van de huisarts overgedragen aan de specialist ouderengeneeskunde. Vanuit de gestelde indicatie blijkt dus grotendeels welke hoofdbehandelaar voor de zorgvrager beschikbaar zal zijn. Daarbij komt: een antwoord op een dergelijke indicator kan alleen opgebouwd worden uit gegevens van al bestaande cliënten van de zorgaanbieder, het is niet met zekerheid te zeggen dat de hoofdbehandelaar in het
specifieke geval van één zorgvrager ook daadwerkelijk straks de hoofdbehandelaar is. De werkgroep heeft besloten deze vraag niet op te nemen omdat het antwoord op deze vraag te weinig zegt.

Indien opportuun kan een vraag over ondersteuning door zorgpersoneel wel tijdens een bezoek aan de locatie door cliënt en familie/naasten gesteld worden:

* Moet ik als cliënt niet te lang wachten als ik hulp nodig heb of vragen heb?

Er zijn eveneens vragen over de locatie afgevallen. Deze vragen spelen minder mee bij de eerste keuze voor een zorglocatie. De vragen spelen wel wat verderop in het keuzeproces, namelijk tijdens het bezoek aan een zorglocatie. Om deze reden worden ze opgenomen in de folder “Kennismaken met het verpleeghuis” die Patiëntenfederatie Nederland heeft ontwikkeld als hulp tijdens het bezoek aan een zorglocatie:

* Kan ik binnen de locatie over de afdelingen heen bewegen?
* Kan de bewoner eten waar hij/ zij wil?
* Kan de bewoner warm eten wanneer hij/zij wil?
* Kan bewoner zelf koken?
* Is er een winkel op de locatie?
* Hoe is de sfeer?
* Kan ik mijn eigen meubilair meenemen?
* Welke activiteiten die worden aangeboden?
* Is er parkeergelegenheid voor bezoekers op eigen terrein?
* Welke financiële consequenties, bv. eigen bijdrage, heeft het wonen op deze locatie?
* Praktische informatie: wat regelt de nieuwe cliënt wat? Wlz? Wat is een zorgprofiel? Waar kan cliënt terecht voor hulp en advies? Hoe ziet eerste bezoek aan zorginstelling eruit? Checklist verhuizing?

Vragen die met Google Maps te beantwoorden zijn (en derhalve niet in het uitwisselprofiel zijn opgenomen):

* Welke voorzieningen zijn er nabij de locatie?
* Wat is het dichtstbijzijnde NS station?
* Op hoeveel meter is de dichtstbijzijnde bushalte?
* Is er openbaar vervoer binnen 500 meter?
* Is er een park binnen 250 meter? Is er een groen gebied binnen 500 meter?
* Wat is de ligging van de locatie: in een dorp of stad, in een buitenwijk of in een centrum
* Is er een winkelgebied in de buurt?

### Algemeen
|Informatievraag|Bestaande/gewijzigde/nieuwe informatievraag|Toelichting|
|---------------|-------------------------------------------|-----------|
|Wat is de naam van de organisatie?; Wat is het KvK-nummer van de organisatie?; Wat is de naam van de vestiging?; Wat is het KvK vestigingsnummer van de vestiging?;Wat is de postcode van de organisatie?;Wat is de postcode van de vestiging?;Wat is de AGB code van de organisatie?;Wat is de AGB code van de vestiging?;Wat is het NZa nummer en wat zijn eventuele bijbehorende NZa nummers?|Nieuwe informatievraag|Op concern- en vestigingsniveau zijn deze gegevens nodig om de juiste organisatie te identificeren en om vestigingsgegevens te kunnen koppelen. Deze gegevens worden ook gebruikt door PFN voor een koppeling met gegevens over wachtlijsten (via NZa). Belangrijk aandachtspunt: op welke termijn moet een nieuwe locatie zijn aangemeld bij het KvK-register? (Zorgkantoren willen zo vroeg mogelijk weten dat er een locatie geopend gaat worden, zodat cliënten al naar deze locatie bemiddeld kunnen worden).|

### Thema: Zorgaanbod
|Informatievraag|Bestaande/gewijzigde/nieuwe informatievraag|Toelichting|
|---------------|-------------------------------------------|-----------|
|1.1.1 Welke zorgprofielen heeft deze vestiging geleverd bij leveringsvorm Verblijf?|Nieuwe informatievraag|De werkgroep heeft aangegeven dat zij inzicht willen hebben in alle zorg die geleverd kan worden op een vestiging. Echter, gegevens over de te leveren zorg (toekomst) worden niet in de systemen bij zorgaanbieders vastgelegd. Daarom is in de werkgroep besloten om toch terug te kijken naar de geleverde zorg (bijv. op basis van declaratiedata). Om ervoor te zorgen dat er zo'n compleet mogelijk beeld ontstaat van wat de vestiging kan leveren (rekening houdend met zorg die in de toekomst geleverd kan worden en zorg die nu wel wordt geleverd maar in de toekomst niet meer), hebben zorgaanbieders de optie om de gegenereerde set gegevens nog aan te passen voor het verstuurd wordt.|
|1.1.2 In welke woonplaats(en) kan deze organisatie zijn welke zorgprofielen geleverd bij leveringsvorm Volledig Pakket Thuis (VPT)? |Nieuwe informatievraag|De werkgroep heeft aangegeven dat zij inzicht willen hebben in alle zorg bij leveringsvorm VPT die geleverd kan worden door een organisatie. Echter, gegevens over de te leveren zorg (toekomst) worden niet in de systemen bij zorgaanbieders vastgelegd. Daarom is in de werkgroep besloten om toch terug te kijken naar de geleverde zorg (bijv. op basis van declaratiedata). Om ervoor te zorgen dat er zo'n compleet mogelijk beeld ontstaat van wat de organisatie kan leveren (rekening houdend met zorg die in de toekomst geleverd kan worden en zorg die nu wel wordt geleverd maar in de toekomst niet meer), hebben zorgaanbieders de optie om de gegenereerde set gegevens nog aan te passen voor het verstuurd wordt.|
|1.1.3 In welke woonplaats(en) is welk zorgaanbod geleverd bij leveringsvorm Modulair Pakket Thuis (MPT)?|Nieuwe informatievraag|De werkgroep heeft aangegeven dat zij inzicht willen hebben in alle zorg bij leveringsvorm MPT die geleverd kan worden door een organisatie. Echter, gegevens over de te leveren zorg (toekomst) worden niet in de systemen bij zorgaanbieders vastgelegd. Daarom is in de werkgroep besloten om toch terug te kijken naar de geleverde zorg (bijv. op basis van declaratiedata). Om ervoor te zorgen dat er zo'n compleet mogelijk beeld ontstaat van wat de organisatie kan leveren(rekening houdend met zorg die in de toekomst geleverd kan worden en zorg die nu wel wordt geleverd maar in de toekomst niet meer), hebben zorgaanbieders de optie om de gegenereerde set gegevens nog aan te passen voor het verstuurd wordt.|
|1.1.4 Welke zorgprofielen heeft deze vestiging geleverd bij de combinatie van leveringsvormen Deeltijdverblijf (DTV)?|Nieuwe informatievraag|De werkgroep heeft aangegeven dat zij inzicht willen hebben in alle zorg die geleverd kan worden op een vestiging. Echter, gegevens over de te leveren zorg (toekomst) worden niet in de systemen bij zorgaanbieders vastgelegd. Daarom is in de werkgroep besloten omtoch terug te kijken naar de geleverde zorg (bijv. op basis van declaratiedata). Om ervoor te zorgen dat er zo'n compleet mogelijk beeld ontstaat van wat de vestiging kan leveren (rekening houdend met zorg die in de toekomst geleverd kan worden en zorg die nu wel wordt geleverd maar in de toekomst niet meer), hebben zorgaanbieders de optie om de gegenereerde set gegevens nog aan te passen voor het verstuurd wordt.DTV (Deeltijdverblijf) is een combinatie van thuis wonen en in een Wlz-instelling wonen. Dit is dus een combinatie van leveringsvormen die de organisatie bereid is te leveren aan één persoon. Concreet zijn er drie combinaties mogelijk: (Verblijf & MPT), (Verblijf & PGB) en (Verblijf & MPT & PGB). Deze indicator beschrijft van deze combinatie de component Verblijf, de beschikbare zorg uit de andere component(en) wordt beschreven in 1.1.3 en 1.1.5|
|1.1.5 In welke woonplaats(en) is zorg thuis middels een Persoonsgebonden Budget (PGB) geleverd?|Nieuwe informatievraag|De werkgroep heeft aangegeven dat zij inzicht willen hebben in alle zorg thuis middels een PGB die geleverd kan worden door een organisatie. Echter, gegevens over de te leveren zorg (toekomst) worden niet in de systemen bij zorgaanbieders vastgelegd. Daarom is in de werkgroep besloten om toch terug te kijken naar de geleverde zorg (bijv. op basis van declaratiedata). Om ervoor te zorgen dat er zo'n compleet mogelijk beeld ontstaat van wat de organisatie kan leveren (rekening houdend met zorg die in de toekomst geleverd kan worden en zorg die nu wel wordt geleverd maar in de toekomst niet meer), hebben zorgaanbieders de optie om de gegenereerde set gegevens nog aan te passen voor het verstuurd wordt.|

### Thema: Wzd/Wvggz
|Informatievraag|Bestaande/gewijzigde/nieuwe informatievraag|Toelichting|
|---------------|-------------------------------------------|-----------|
|1.4.1 Welke vestigingen zijn als Wzd- of Wvggz-vestiging geregistreerd?|Nieuwe informatievraag|Deze gegevens worden niet direct bij de zorgaanbieder opgevraagd, maar bij het Wzd-locatieregister.|
|1.4.2 Welke vestigingen zijn als Wzd- of Wvggz-accommodatie geregistreerd?|Nieuwe informatievraag|Deze gegevens worden niet direct bij de zorgaanbieder opgevraagd, maar bij het Wzd-locatieregister.|
  
### Thema: Huurprijs wooneenheden
|Informatievraag|Bestaande/gewijzigde/nieuwe informatievraag|Toelichting|
|---------------|-------------------------------------------|-----------|
|1.5.1 Wat is het aantal wooneenheden onder en boven de sociale huurgrens bij deze vestiging?|Nieuwe informatievraag|In eerste instantie was de indicator zo opgebouwd dat er werd gevraagd of er minimaal één wooneenheid onder de huurgrens aanwezig is. Echter, het antwoord op deze vraag geeft te weinig informatie voor een zorgvrager. Daarom is de vraag aangepast naar het aantal wooneenheden onder en boven de sociale huurgrens.|
|1.5.2 Wat is het aantal wooneenheden per huurprijscategorie?|Nieuwe informatievraag|In eerste instantie ging de vraag over de minimale en maximale huurprijsen servicekosten. Door de werkgroep is aangegeven dat het vooral van belang is om inzichtelijk te maken in welke categorieën woningen worden aangeboden en welke servicekosten daarbij horen. Om die reden is de vraag aangepast en wordt er gevraagd naar aantallen per categorie.|

### Thema: Capaciteit
|Informatievraag|Bestaande/gewijzigde/nieuwe informatievraag|Toelichting|
|---------------|-------------------------------------------|-----------|
|3.1 Wat is het aantal personen dat per vestiging kan wonen (capaciteit)?|Nieuwe informatievraag|Het aantal personen per vestiging (capaciteit) zegt iets over de grootte van de vestiging. Een zorgvrager kan zo inschatten of hij/zij zich prettig voelt bij deze schaalgrootte.|

### Thema: Overig
|Informatievraag|Bestaande/gewijzigde/nieuwe informatievraag|Toelichting|
|---------------|-------------------------------------------|-----------|
|99.1 In welke mate wordt rekening gehouden met de wensen rondom levenseinde door deze vestiging?|Bestaande informatievraag|Uitwisselprofiel Zorginstituut ODB, indicator 2. Indien deze indicator niet meer wordt opgenomen in de integrale meetinstrumenten van het Kwaliteitskompas, zal deze ook niet meer in dit uitwisselprofiel worden uitgevraagd.|
|99.2 In welke mate wordt rekening gehouden met de voedselvoorkeuren van de cliënt door deze vestiging?|Bestaande informatievraag|Uitwisselprofiel Zorginstituut ODB, indicator 6. Indien deze indicator niet meer wordt opgenomen in de integrale meetinstrumenten van het Kwaliteitskompas, zal deze ook niet meer in dit uitwisselprofiel worden uitgevraagd.|
|99.3 Wat is de link naar het laatste Kwaliteitsverslag van de organisatie?|Bestaande informatievraag|Uitwisselprofiel Zorginstituut ODB, indicator 7. Indien deze indicator niet meer wordt opgenomen in de integrale meetinstrumenten van het Kwaliteitskompas, zal deze ook niet meer in dit uitwisselprofiel worden uitgevraagd.|

### Privacytoets
Door de Privacy Company is een toets uitgevoerd om inzicht te krijgen in potentiële privacyrisico’s. Er is o.a. gekeken naar de
grondslag van zorgkantoren om gegevens uit te vragen, er is een analyse gedaan op de gestelde vragen en er is gekeken naar de
proportionaliteit van de gestelde vragen. Ook is hierbij rekening gehouden met eventuele herleidbaarheid naar individuele personen.

Omdat er geen persoonsgegevens verwerkt worden, is de uitvraag proportioneel. Ook doorlevering aan de Patiëntenfederatie Nederland ten behoeve van de Zorgkaart en Keuzehulp Verpleeghuiszorg is proportioneel.

Bij de uitvraag op woonplaatsniveau kan mogelijk indirecte herleidbaarheid ontstaan. Met name in kleine woonplaatsen of bij kleine vestgingen kunnen de aantallen patiënten dusdanig laag zijn dat duidelijk is om wie het zou gaan. In dat geval is echter nog steeds aanvullende informatie nodig om daadwerkelijk tot identificatie of herleidbaarheid te komen. Op basis van alleen de indicatoren is dit niet mogelijk.

Binnen dit uitwisselprofiel gaan alle vragen over de zorg die een instelling daadwerkelijk levert. Zonder aanvullende gegevens is er echter geen sprake van herleidbaarheid en is het risico voor een inbreuk op de privacy van patiënten minimaal.


