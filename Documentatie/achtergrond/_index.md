---
title: Achtergrond
weight: 6
---
# Achtergrond
Deze sectie bevat achtergrondinformatie, aanvullende toelichting bij het Uitwisselprofiel.

Hierin wordt aangegeven welke keuzes er zijn gemaakt in de vraagstelling of het antwoord, dan wel in het gebruik van de concepten in de modelgegevensset. Op die manier is duidelijk waarop de uitwerking in het uitwisselprofiel is gebaseerd. Deze informatie dient als achtergrond. 

* [Toelichting](/Documentatie/achtergrond/toelichting/_index.md)