---
title: 1.5.1 Aantal wooneenheden onder en boven de sociale huurgrens
description: "ntb"
weight: 10
---

# DEZE BESCHRIJVING IS IN ONTWIKKELING

## Definitie

**Definitie:** Op meetmoment per vestiging en de organisatie als geheel het aantal wooneenheden boven en onder de sociale huurgrens.


## Toelichting

De informatievraag betreft per vestiging en de organisatie als geheel het aantal wooneenheden boven en onder de sociale huurgrens.


## Uitgangspunten

* Een sociale huurwoning kent een begrensde huur. De huur mag nooit hoger zijn dan een bepaald bedrag. Dit heet de maximale huurgrens van de woning. 
* De sociale huurgrens betreft een rekenhuur van maximaal € 808,06 (Dit is de grens in 2023). De rekenhuur is de kale huur plus de servicekosten. Niet alle servicekosten tellen mee, alleen de 4 servicekostenposten die meegenomen worden in de huurtoeslag. Het gaat om 1 of meer van de volgende servicekosten:
    - kosten voor schoonmaak van gemeenschappelijke ruimten;
    - kosten energie voor gemeenschappelijke ruimten;
    - kosten voor de huismeester;
    - kosten voor dienst- en recreatieruimten.

Bronnen: 

https://www.rijksoverheid.nl/onderwerpen/huurwoning-zoeken/vraag-en-antwoord/wat-is-het-verschil-tussen-een-sociale-huurwoning-en-een-huurwoning-in-de-vrije-sector#:~:text=Huurwoning%20en%20huurtoeslag&text=Dat%20kan%20als%20uw%20rekenhuur,de%20servicekosten%20die%20u%20betaalt.

https://www.belastingdienst.nl/wps/wcm/connect/bldcontentnl/belastingdienst/prive/toeslagen/huurtoeslag/huur-en-servicekosten/wat-is-rekenhuur


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle wooneenheden.
2. Bepaal per wooneenheid de vestiging de rekenhuur.
3. Bepaal op basis van de rekenhuur per wooneenheid of deze onder of boven de sociale huurgrens valt.
4. Bereken per vestiging en voor de organisatie als geheel het totaal aantal wooneenheden, het aantal wooneenheden dat onder de sociale huurgrens valt en het aantal wooneenheden dat boven de sociale huurgrens valt.

Peildatum: dd-mm-jjjj

| Organisatieonderdeel | Aantal wooneenheden | Aantal wooneenheden onder de sociale huurgrens | Aantal wooneenheden boven de sociale huurgrens |
| ---  | --- | --- | --- | 
| Organisatie | Stap 4 | Stap 4 | Stap 4 | 
| Vestiging 1 | Stap 4 | Stap 4 | Stap 4 | 
| Vestiging 2 | Stap 4 | Stap 4 | Stap 4 | 
| Vestiging N | Stap 4 | Stap 4 | Stap 4 | 
