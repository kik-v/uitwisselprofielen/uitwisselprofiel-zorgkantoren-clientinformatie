---
title: 1.1.5 In welke woonplaats(en) is zorg thuis middels een PersoonsGebonden Budget (PGB) geleverd?
description: "ntb"
weight: 5
---

## Indicator

**Definitie:** In meetperiode geleverde zorg thuis bij leveringsvorm PersoonsGebonden Budget (PGB) per woonplaats.

## Toelichting

De informatievraag betreft de woonplaatsen waar zorg thuis middels een PersoonsGebonden Budget (PGB) geleverd is in de meetperiode.

## Uitgangspunten

* Uitwerking binnen het kader van de Wlz.
* In verband met de efficientie van de gegvensverwerking wordt de geleverde zorg bepaald op basis van indicatiebesluiten, AW33 bericht "Toewijzing zorg". Aangenomen wordt daarbij dat die zorg ook werkelijk geleverd is.
* Alleen declaraties bij leveringsvorm PGB worden geïncludeerd.
* Alle prestaties zoals beschreven in [Beleidsregel prestatiebeschrijvingen en tarieven modulaire zorg 2024 - BR/REG-24122a](https://puc.overheid.nl/nza/doc/PUC_750693_22/) met uitzondering van de prestaties onder punt "5. Behandeling individueel en behandeling in een groep (dagbehandeling)" worden geïncludeerd. Concreet zijn de volgende prestatiecodes uitgesloten: H335, , H336, H329, H330, H325, H334, H338, H331, H332, H333, H353, H354, H355, H356, H357, H802, H804, H819, H820, H821, H817, H822, H891, H840. Deze prestaties zijn gelijk aan de prestaties in de regeling voor 2023.
* Een indicatie valt in de meetperiode als er minimaal 1 dag overlap zit tussen de indicatieperiode (startdatum tot en met einddatum) en de meetperiode.
* De woonplaats waar een prestatie geleverd is wordt bepaald op basis van de postcode van het woonadres van de betreffende verzekerde.
* De meetperiode betreft 1-jan-2023 t/m 31-dec-2023.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle indicatiebesluiten waarbij
    * de leveringsvorm PGB is en
    * de geleverde prestatie voorkomt in [Beleidsregel prestatiebeschrijvingen en tarieven modulaire zorg 2024 - BR/REG-24122a](https://puc.overheid.nl/nza/doc/PUC_750693_22/) met uitzondering van de prestaties H335, , H336, H329, H330, H325, H334, H338, H331, H332, H333, H353, H354, H355, H356, H357, H802, H804, H819, H820, H821, H817, H822, H891, H840 en
    * de indicatieperiode overlapt met de meetperiode.
2. Bepaal van ieder indicatiebesluit de woonplaats incl. gemeente waar de zorg is geleverd.
3. Rapporteer iedere woonplaats incl. gemeente die minimaal eenmaal voorkomt.
