---
title: 1.1.3 In welke woonplaats(en) is welk zorgaanbod geleverd bij leveringsvorm Modulair Pakket Thuis (MPT)?
description: "ntb"
weight: 3
---

## Indicator

**Definitie:** Op meetmoment beschikbaar zorgaanbod bij leveringsvorm Modulair Pakket Thuis (MPT) per woonplaats.

## Toelichting

De informatievraag betreft het zorgaanbod uitgedrukt in groepen zorgprestaties bij leveringsvorm Modulair Pakket Thuis (MPT) die per woonplaats geleverd zijn in de meetperiode.

## Uitgangspunten

* Uitwerking binnen het kader van de Wlz.
* In verband met de efficientie van de gegvensverwerking wordt de geleverde zorg bepaald op basis van indicatiebesluiten, AW33 bericht "Toewijzing zorg". Aangenomen wordt daarbij dat die zorg ook werkelijk geleverd is.
* Alleen indicatiebesluiten betreffende leveringsvorm MPT worden geïncludeerd.
* Alle prestaties zoals beschreven in [Beleidsregel prestatiebeschrijvingen en tarieven modulaire zorg 2024 - BR/REG-24122a](https://puc.overheid.nl/nza/doc/PUC_750693_22/) met uitzondering van de prestaties onder punt "5. Behandeling individueel en behandeling in een groep (dagbehandeling)" worden geïncludeerd. Deze prestaties zijn gelijk aan de prestaties in de regeling voor 2023. De prestatiecodes worden gegroepeerd in "zorgaanbod" zoals hieronder beschreven.
  * **Huishoudelijke hulp** (H117)
  * **Persoonlijke verzorging** (H126, H127, H138, H120)
  * **Begeleiding individueel** (H300, H306, H150, H152, H153, H301, H303, H302, H304, H132, H180)
  * **Verpleging** (H104)
  * **Thuizorgtechnologie ten behoeve van verpleging** (H139)
  * **Verpleging incl. beschikbaarheid** (H128)
  * **Verpleging speciaal** (H106)
  * **Verpleging speciaal aan kinderen tot 18 jaar incl. beschikbaarheid** (H118)
  * **Verpleging speciaal aan kinderen tot 18 jaar excl. Beschikbaarheid** (H119)
  * **Vervoer (naar begeleiding en/of behandeling)** (H8030, H8031, H8032, H8033, H8034, H8035, H8036, , H886, H881, H882, H883, H884, H885, H887, H410, H411, H412, H413, H414, H415, H416)
* Prestaties die niet voorkomen in bovenstaande overzicht worden niet gerapporteerd.
* Een indicatie valt in de meetperiode als er minimaal 1 dag overlap zit tussen de indicatieperiode (startdatum tot en met einddatum) en de meetperiode.
* De woonplaats waar een prestatie geleverd is wordt bepaald op basis van de postcode van het woonadres van de betreffende verzekerde.
* De meetperiode betreft 1-jan-2023 t/m 31-dec-2023.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle indicatiebesluiten waarbij:
    * de leveringsvorm MPT is en
    * de geleverde prestatie voorkomt in [Beleidsregel prestatiebeschrijvingen en tarieven modulaire zorg 2024 - BR/REG-24122a](https://puc.overheid.nl/nza/doc/PUC_750693_22/) en
    * de indicatieperiode overlapt met de meetperiode.
2. Bepaal (op basis van deze indicatiebesluiten) per zorgaanbod in welke woonplaatsen incl. gemeente minimaal eenmaal deze zorg is geleverd.
3. Rapporteer per woonplaats incl. gemeente welk zorgaanbod geleverd is. Woonplaatsen incl. gemeente waar geen zorg geleverd is hoeven niet gerapporteerd te worden.
