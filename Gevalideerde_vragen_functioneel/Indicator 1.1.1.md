---
title: 1.1.1 Welke zorgprofielen heeft deze vestiging geleverd bij leveringsvorm Verblijf?
description: "ntb"
weight: 1
---

## Indicator

**Definitie:** In meetperiode geleverde zorgprofielen bij leveringsvorm Verblijf per vestiging.

## Toelichting

De informatievraag betreft de zorgprofielen bij leveringsvorm Verblijf die per vestiging geleverd zijn in de meetperiode.

## Uitgangspunten

* Uitwerking binnen het kader van de Wlz.
* In verband met de efficientie van de gegvensverwerking wordt de geleverde zorg bepaald op basis van indicatiebesluiten, AW33 bericht "Toewijzing zorg". Aangenomen wordt daarbij dat die zorg ook werkelijk geleverd is.
* Alleen indicatiebesluiten betreffende leveringsvorm Verblijf worden geïncludeerd.
* Alle zorgprofielen binnen de sectoren VV en LG zoals beschreven in [Bijlage 1 bij Beleidsregel prestatiebeschrijvingen en tarieven zorgzwaartepakketten en volledig pakket thuis 2024](https://puc.overheid.nl/PUC/Handlers/DownloadBijlage.ashx?pucid=PUC_755423_22_1&bestand=Bijlage_1_bij_BR-REG-24123b_Overzicht_zorgprofielen_en_bijbehorende_zzp%27s.pdf&bestandsnaam=Bijlage+1+bij+BR-REG-24123b+Overzicht+zorgprofielen+en+bijbehorende+zzp%27s.pdf) worden geïncludeerd, inclusief de zorgprofielen in overgangsregeling (vv-1, vv-2, vv-3, lg-1 en lg-3). Deze zorgprofielen zijn gelijk aan de zorgprofielen in de regeling voor 2023.
* Een indicatie valt in de meetperiode als er minimaal 1 dag overlap zit tussen de indicatieperiode (startdatum tot en met einddatum) en de meetperiode.
* De meetperiode betreft 1-jan-2023 t/m 31-dec-2023.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle indicatiebesluiten waarbij
    * de leveringsvorm Verblijf is en
    * het geleverde zorgprofiel de sector VV of LG betreft en
    * de indicatieperiode overlapt met de meetperiode.
2. Bepaal van ieder indicatiebesluit de vestiging waar de cliënt verblijft.
3. Rapporteer per vestiging welk(e) zorgprofiel(en) minimaal eenmaal zijn geïndiceerd.
