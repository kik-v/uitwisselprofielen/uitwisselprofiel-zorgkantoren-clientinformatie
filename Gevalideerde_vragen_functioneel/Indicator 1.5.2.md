---
title: 1.5.2 Aantal wooneenheden per huurprijscategorie
description: "ntb"
weight: 11
---

# DEZE BESCHRIJVING IS IN ONTWIKKELING


## Definitie

**Definitie:** Op meetmoment per vestiging en de organisatie als geheel de huurprijzen van wooneenheden per huurprijscategorie.

## Toelichting

De informatievraag betreft per vestiging en de organisatie als geheel de huurprijzen van wooneenheden per huurprijscategorie.

## Uitgangspunten

* De huurprijs bereft de kale huur plus de servicekosten (ongeacht of de betreffende servicekosten meegerekend worden voor het bepalen van de sociale huurgrens).
* Een sociale huurwoning kent een begrensde huur. De huur mag nooit hoger zijn dan een bepaald bedrag. Dit heet de maximale huurgrens van de woning.
* De sociale huurgrens betreft een rekenhuur van maximaal € 808,06 (Dit is de grens in 2023). De rekenhuur is de kale huur plus de servicekosten. Niet alle servicekosten tellen mee, alleen de 4 servicekostenposten die meegenomen worden in de huurtoeslag. Het gaat om 1 of meer van de volgende servicekosten:
    - kosten voor schoonmaak van gemeenschappelijke ruimten;
    - kosten energie voor gemeenschappelijke ruimten;
    - kosten voor de huismeester;
    - kosten voor dienst- en recreatieruimten.

Bronnen:
https://www.rijksoverheid.nl/onderwerpen/huurwoning-zoeken/vraag-en-antwoord/wat-is-het-verschil-tussen-een-sociale-huurwoning-en-een-huurwoning-in-de-vrije-sector#:~:text=Huurwoning%20en%20huurtoeslag&text=Dat%20kan%20als%20uw%20rekenhuur,de%20servicekosten%20die%20u%20betaalt.

https://www.belastingdienst.nl/wps/wcm/connect/bldcontentnl/belastingdienst/prive/toeslagen/huurtoeslag/huur-en-servicekosten/wat-is-rekenhuur


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle wooneenheden.
2. Bepaal per wooneenheid de vestiging en de huurprijs inclusief servicekosten.
3. Bepaal per vestiging en voor de organisatie als geheel het aantal wooneenheden per prijscategorie.

Peildatum: dd-mm-jjjj

| Organisatieonderdeel | Aantal wooneenheden met een huurprijs (incl. servicekosten) tot Euro 800,- | Aantal wooneenheden met een huurprijs (incl. servicekosten) van Euro 800,- tot Euro 1.200,- | Aantal wooneenheden met een huurprijs (incl. servicekosten) van Euro 1.200,- tot Euro 2.000,- | Aantal wooneenheden met een huurprijs (incl. servicekosten) vanaf Euro 2.000,- |  
| ---  | --- | --- | --- | --- | 
| Organisatie | Stap 3 | Stap 3 | Stap 3 | Stap 3 | 
| Vestiging 1 | Stap 3 | Stap 3 | Stap 3 | Stap 3 |  
| Vestiging 2 | Stap 3 | Stap 3 | Stap 3 | Stap 3 |   
| Vestiging N | Stap 3 | Stap 3 | Stap 3 | Stap 3 |  
