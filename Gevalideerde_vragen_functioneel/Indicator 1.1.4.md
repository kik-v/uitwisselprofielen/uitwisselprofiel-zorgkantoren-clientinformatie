---
title: 1.1.4 Welke zorgprofielen heeft deze vestiging geleverd bij de combinatie van leveringsvormen Deeltijdverblijf?
description: "ntb"
weight: 4
---

## Indicator

**Definitie:** In meetperiode geleverde zorgprofielen bij de combinatie van leveringsvormen Deeltijdverblijf per vestiging.

## Toelichting

De informatievraag betreft de zorgprofielen bij de combinatie van leveringsvormen Deeltijdverblijf (DTV) die per vestiging geleverd zijn in de meetperiode. DTV is een combinatie van leveringsvormen. Er zijn concreet drie combinaties mogelijk: Verblijf & MPT, Verblijf & PGB en Verblijf & MPT & PGB.

## Uitgangspunten

* Uitwerking binnen het kader van de Wlz.
* In verband met de efficientie van de gegvensverwerking wordt de geleverde zorg bepaald op basis van indicatiebesluiten, AW33 bericht "Toewijzing zorg". Aangenomen wordt daarbij dat die zorg ook werkelijk geleverd is.
* Alleen indicatiebesluiten betreffende leveringsvorm Verblijf worden geïncludeerd waarbij t.b.v. de betreffende cliënt in dezelfde indicatieperiode ook een indicatiebesluit voor MPT en/of PGB bestaat.
* Onderstaande zorgprofielen binnen de sectoren VV en LG zoals beschreven in [Bijlage 1 bij Beleidsregel prestatiebeschrijvingen en tarieven zorgzwaartepakketten en volledig pakket thuis 2024](https://puc.overheid.nl/PUC/Handlers/DownloadBijlage.ashx?pucid=PUC_755423_22_1&bestand=Bijlage_1_bij_BR-REG-24123b_Overzicht_zorgprofielen_en_bijbehorende_zzp%27s.pdf&bestandsnaam=Bijlage+1+bij+BR-REG-24123b+Overzicht+zorgprofielen+en+bijbehorende+zzp%27s.pdf) worden geïncludeerd. Deze zorgprofielen zijn gelijk aan de zorgprofielen in de regeling voor 2023.
  * VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging (vv-4)
  * VV Beschermd wonen met intensieve dementiezorg (vv-5)
  * VV Beschermd wonen met intensieve verzorging en verpleging (vv-6)
  * VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding (vv-7)
  * VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging (vv-8)
  * LG Wonen met begeleiding en enige verzorging (lg-2)
  * LG Wonen met begeleiding en verzorging (lg-4)
  * LG Wonen met begeleiding en intensieve verzorging (lg-5)
  * LG Wonen met intensieve begeleiding en intensieve verzorging (lg-6)
  * LG Wonen met zeer intensieve begeleiding en zeer intensieve verzorging (lg-7)
* Zorgprofielen die niet voorkomen in bovenstaande overzicht worden niet gerapporteerd.
* Een indicatie valt in de meetperiode als er minimaal 1 dag overlap zit tussen de indicatieperiode (startdatum tot en met einddatum) en de meetperiode.
* De meetperiode betreft 1-jan-2023 t/m 31-dec-2023.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle indicatiebesluiten waarbij
    * de leveringsvorm Verblijf is en in de indicatieperiode voor dezelfde cliënt tevens een indicatiebesluit met leveringsvorm MPT en/of PGB bestaat en
    * het geleverde zorgprofiel in bovengenoemde lijst onder uitgangspunten voorkomt en
    * de indicatieperiode overlapt met de meetperiode.
2. Bepaal van ieder indicatiebesluit de vestiging waar de cliënt verblijft.
3. Rapporteer per vestiging welk(e) zorgprofiel(en) minimaal eenmaal zijn geïndiceerd.
