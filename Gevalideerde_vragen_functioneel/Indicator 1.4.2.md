---
title: 1.4.2 Welke vestigingen zijn als Wzd- of Wvggz-accommodatie geregistreerd?
description: "ntb"
weight: 9
---

## Definitie

**Definitie:** Op de peildatum per vestiging of de betreffende vestiging als Wzd- en/of Wvggz-accommodatie is geregistreerd in het locatieregister.

## Toelichting

De informatievraag betreft welke vestigingen op de peildatum als Wzd- en/of Wvggz-accommodatie staan geregistreerd in het locatieregister.

## Uitgangspunten

* Of een vestiging een WZD- en/of Wvggz-accommodatie betreft wordt bepaald o.b.v. de registratie van de vestiging als WZD- of Wvggz-accommodatie in het locatie-register. NB: In het kennismodel van KiK-V wordt een 'locatie' een 'vestiging' genoemd. _Een vestiging is een gebouw of complex van gebouwen waar duurzame uitoefening van de activiteiten van een onderneming of rechtspersoon plaatsvindt_.
* Een accommodatie binnen de Wzd of Wvggz is een vestiging (locatie in de Wzd terminologie) waar betrokkenen gedwongen opgenomen kunnen worden, of met een opname op grond van artikel 21 (opname en verblijf op grond van een besluit van het CIZ) van de Wzd kunnen verblijven [(Notitie 'De betekenis van locatie en accommodatie binnen de Wvggz en de Wzd')](https://www.dwangindezorg.nl/documenten/publicaties/implementatie/wzd/diversen/notitie-vws---de-betekenis-van-locatie-en-accommodatie-binnen-de-wzd-en-wvggz).

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle vestigingen.
2. Bepaal per vestiging of deze op de peildatum als Wzd- en/of Wvggz-accommodatie is geregistreerd. Rapporteer als dat het geval is per wet 'ja', en anders per wet 'nee'.

Peildatum: dd-mm-jjjj

| Organisatieonderdeel | geregistreerd als Wzd-vestiging? | geregistreerd als Wvggz-vestiging? |
| --- | --- | --- |
| Vestiging 1 | stap 2 | stap 2 |
| Vestiging 2 | stap 2 | stap 2 |
| Vestiging N | stap 2 | stap 2 |
