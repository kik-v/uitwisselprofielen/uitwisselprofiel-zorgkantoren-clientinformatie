# A pipeline is composed of independent jobs that run scripts, grouped into stages.
# Stages run in sequential order, but jobs within stages run in parallel.

stages: #List of stages for jobs, and their order of execution
  - queryvalidation
  - datevalidation
  - tablevalidation
  - indexgeneration
  - sparql2md
  - urlvalidation
  - getrelease

image: python:latest

variables:
  VERSION: ""
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_REGISTRY_FUSEKI: registry.gitlab.com/kik-v/ontologie/source/fuseki:main
  DOCKER_REGISTRY_GRAPHDB: registry.gitlab.com/kik-v/ontologie/source/graphdb:main

.before_script_queryvalidation:
  before_script:
    - apk add --update git py-pip
    - python3 -m venv pipelineVenvKikV
    - source pipelineVenvKikV/bin/activate
    - pip install zorginstituut --index-url https://$PACKAGE_TOKEN_NAME:$PACKAGE_TOKEN@gitlab.com/api/v4/projects/46247986/packages/pypi/simple

.before_script_changelog:
  before_script:
    - mkdir ~/.ssh/
    - echo "${CI_KNOWN_HOSTS}" > ~/.ssh/known_hosts
    - echo "${SSH_PUSH_KEY}" > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
    - git config user.email "ci@example.nl"
    - git config user.name "CI-bot"
    - git remote remove origin || true # Local repo state may be cached
    - git remote add origin "git@${CI_SERVER_HOST}:${CI_PROJECT_PATH}.git"

.before_script:
  before_script:
    - mkdir ~/.ssh/
    - echo "${CI_KNOWN_HOSTS}" > ~/.ssh/known_hosts
    - echo "${SSH_PUSH_KEY}" > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
    - git config user.email "ci@example.nl"
    - git config user.name "CI-bot"
    - git remote remove origin || true # Local repo state may be cached
    - git remote add origin "git@${CI_SERVER_HOST}:${CI_PROJECT_PATH}.git"
    - pip install zorginstituut --index-url https://$PACKAGE_TOKEN_NAME:$PACKAGE_TOKEN@gitlab.com/api/v4/projects/46247986/packages/pypi/simple

queryvalidation:
  image: docker:latest
  stage: queryvalidation
  # tags:
  #   - docker
  extends: .before_script_queryvalidation
  services:
    - docker:dind
    - name: $DOCKER_REGISTRY_FUSEKI
      alias: fusekihost
    - name: $DOCKER_REGISTRY_GRAPHDB
      alias: graphdbhost
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $DOCKER_REGISTRY_GRAPHDB
    - docker pull $DOCKER_REGISTRY_GRAPHDB
    - docker run -p 7200:8080 --name $CI_PIPELINE_ID -d $DOCKER_REGISTRY_GRAPHDB
    - CONTAINER_ID=$(docker inspect --format="{{.Id}}" $CI_PIPELINE_ID) # Get the container_ID
    - mkdir common_files
    - docker container cp ${CONTAINER_ID}:common-files/. common_files/
    - docker stop $CI_PIPELINE_ID
    - apk add --update curl && rm -rf /var/cache/apk/*
    - apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
    - |
      curl -X POST --header "Content-Type:multipart/form-data" -F "config=@./common_files/graphdb-repo-config.ttl" "http://graphdbhost:7200/rest/repositories"
    - |
      for file in common_files/*.owl; do
        curl -X POST -H "Content-Type:application/rdf+xml" --data-binary "@$file" "http://graphdbhost:7200/repositories/kikv_test/statements"
      done
    - |
      for file in common_files/*.ttl; do
        if [ "$file" != "common_files/graphdb-repo-config.ttl" ]; then
          curl -X POST -H "Content-Type:application/x-turtle" --data-binary "@$file" "http://graphdbhost:7200/repositories/kikv_test/statements"
        fi
      done
    - echo "Start validating the sparql queries with GraphDB & Fuseki..."
    - python -c "from pathlib import Path; from zorginstituut import run_queries; run_queries.main(Path('/builds/kik-v/uitwisselprofielen/${CI_PROJECT_NAME}/Gevalideerde_vragen_technisch'))"
    - echo "GraphDB & Fuseki validation completed"
  only:
    - dev
    - acc
    - main

datevalidation:
  image: python:latest
  stage: datevalidation
  tags:
    - saas-linux-medium-amd64
  extends: .before_script
  script:
    - echo "Start validating the startingDate in publicatieplatform-directives.json..."
    - git pull origin ${CI_COMMIT_REF_NAME}
    - python -c 'from zorginstituut import check_date; check_date.main()'
    - echo "Validation completed "
  only:
    - acc
    - main

tablevalidation:
  image: python:latest
  stage: tablevalidation
  tags:
    - saas-linux-medium-amd64
  extends: .before_script
  script:
    - echo "Start validating the tables in the .md files..."
    - git pull origin ${CI_COMMIT_REF_NAME}
    - python -c 'from zorginstituut import table_validation; table_validation.main()'
    - echo "Validation completed"
  only:
    - dev
    - acc
    - main

index-job:
  image: python:latest
  stage: indexgeneration
  tags:
    - saas-linux-medium-amd64
  extends: .before_script
  script:
    - echo "Start generating the index file..."
    - git pull origin ${CI_COMMIT_REF_NAME}
    - python -c 'from zorginstituut import indexfile_generator; indexfile_generator.main()'
    # prevent triggering pipeline again, use $CI_COMMIT_REF_NAME if you want to push to current branch
    - if [[ -n $(git status --porcelain) ]]; then git add Gevalideerde_vragen_technisch/ Gevalideerde_vragen_functioneel/ ; git commit -m "push _index.md files from CI/CD pipeline"; git push origin HEAD:${CI_COMMIT_REF_NAME} -o ci.skip; fi
  only:
    - acc
    - main

sparql-job:
  image: python:latest
  stage: sparql2md
  tags:
    - saas-linux-medium-amd64
  extends: .before_script
  script:
    - echo "Start generating the index file..."
    - git pull origin ${CI_COMMIT_REF_NAME}
    - python -c 'from zorginstituut import sparql2mds; sparql2mds.main()'
    # prevent triggering pipeline again, use $CI_COMMIT_REF_NAME if you want to push to current branch
    - if [[ -n $(git status --porcelain) ]]; then git add Gevalideerde_vragen_technisch/ ; git commit -m "push Gevalideerde_vragen_technisch map from CI/CD pipeline"; git push origin HEAD:${CI_COMMIT_REF_NAME} -o ci.skip ; fi
  only:
    - acc
    - main

urlvalidation:
  image: python:latest
  stage: urlvalidation
  tags:
    - saas-linux-medium-amd64
  extends: .before_script
  script:
    - echo "Start validating the URLs in the .md files..."
    - git pull origin ${CI_COMMIT_REF_NAME}
    - python -c 'from zorginstituut import  URL_validation; URL_validation.main()'
    - echo "Validation completed"
  only:
    - acc
    - main

getrelease:
  image: python:latest
  stage: getrelease
  tags:
    - saas-linux-medium-amd64
  extends: .before_script
  script:
    - git pull origin ${CI_COMMIT_REF_NAME}
    - export VERSION=$(python -c 'from zorginstituut import utils; print(utils.get_release_version())')
    - echo ${VERSION}
    - mkdir output
    - git tag -a ${VERSION} -m "create new version"
    - git push --tags -o ci.skip
  only:
    - acc
    - main
    